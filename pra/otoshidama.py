n, y = map(int, input().split())
flg = False
out = [-1, -1, -1]

for i in range(n+1):
    for j in range(n-i+1):
        if i * 10000 + j * 5000 + (n-i-j)* 1000 == y:
            out = [i, j, n-i-j]
            flg = True
            break
    if flg:
        break

print("{} {} {}".format(out[0], out[1], out[2]))
