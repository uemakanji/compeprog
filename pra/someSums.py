n, a, b = map(int, input().split())
result = 0

for i in range(1, n+1):
    s = [int(x) for x in list(str(i))]
    r = 0
    for j in s:
        r = r + j
    if a  <= r <= b:
        result = result + i

print(result)