n = int(input())
d = [int(input()) for _ in range(n)]
d.sort()
count = 0
temp = 0

for i in d:
    if temp < i:
        count = count + 1
        temp = i

print(count)