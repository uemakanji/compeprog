n = int(input())
count = 0
flg = False
a = list(map(int, input().split()))

while True:
    i = 0
    for num in a:
        if num % 2 == 0:
            a[i] = num / 2
        else:
            flg = True
            break
        i = i + 1
    if flg:
        break
    count = count + 1

print(count)