n = int(input())
t = [int(input()) for _ in range(n)]

print(sorted(t).pop(0))
