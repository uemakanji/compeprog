c = []
for _ in range(4):
    c.append(list(input().split()))

for i in c:
    i.reverse()
c.reverse()

for i in c:
    print(" ".join(i))
