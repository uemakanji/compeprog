n = int(input()) % 30 # 30通りあるから
j = list(map(str, range(1, 7)))
 
for i in range(n):
    r1 = i % 5
    r2 = i % 5 + 1
    j[r1], j[r2] = j[r2], j[r1]
 
print(int("".join(j)))
