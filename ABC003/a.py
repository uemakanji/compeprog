n = int(input())
moneys = [i * 10000 for i in range(1, n+1)]
s = 0
a = 0

for money in moneys:
    s = s + money

print(int(s / n))
