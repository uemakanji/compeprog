a, b = map(int, input().split())
result = 0

if b % a == 0:
    result = a + b
else:
    result = b - a

print(result)