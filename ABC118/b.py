n, m = map(int, input().split())

l = [list(map(int, input().split()))[1:] for _ in range(n)]

c = 0
for i in range(1, m+1):
    g = True
    for j in range(n):
        if i not in l[j][0:]:
            g = False
    if g:
        c = c + 1

print(c)