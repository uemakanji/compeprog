#import math
#from functools import reduce
n = int(input())
a = list(map(int, input().split()))

#print(reduce(math.gcd, a))

b=a[0]
c=a[1]
result=1
for i in range(1,n):
    while 1:
        if b%c == 0:
            result = c
            break
        else:
            r = b%c
            b = c
            c = r
    b=result
    c=a[i]
 
print(result)